---
layout: markdown_page
title: "FY22-Q4 OKRs"
description: "View GitLabs Objective-Key Results for FY22 Q4. Learn more here!"
canonical_path: "/company/okrs/fy22-q4/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from November 1, 2021 to January 31, 2022.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2021-09-27 | CEO shares top goals with E-group for feedback |
| -4 | 2021-10-04 | CEO pushes top goals to this page |
| -4 | 2021-10-04 | E-group propose OKRs for their functions in [Epics and Issues nested under the CEO's OKRs](/company/okrs/#executives-propose-okrs-for-their-functions). These issues and epics are shared in #okrs Slack channel |
| -3 | 2021-10-11 | E-group 50 minute draft review meeting |
| -2 | 2021-10-18 | E-group discusses with their respective teams and polish their OKRs |
| -1 | 2021-10-25 | CEO reports post links to final OKR Epics in #okrs slack channel and @ mention the CEO and CoS for approval |
| 0  | 2021-11-01 | CoS updates OKR page for current quarter to be active |


## OKRs

### 1. CEO: Encourage wider community engagement
   1. **CEO KR:** Develop a strategy to grow to 1000 contributors per month
   1. **CEO KR:** Meet quarterly objectives for hyperscalers and partners
   1. **CEO KR:** Achieve 7 certifications with each more than 2,500 certificates issued

### 2. CEO: Optimize GitLab Managed future
   1. **CEO KR:** Meet GitLab.com improvement goals (availability above 99.95%, free user RoI strategy, increase trial conversion from x% to y% on the path to z%)
+   1. **CEO KR:** Meet SaaS improvement goals (increase customer empathy project in engineering, x customers live with Project Horse beta, launch storage visibility)
   1. **CEO KR:** Implement category creation plan

### 3. CEO: Accelerate customer initiatives
   1. **CEO KR:** Improve SUS usability through completing 100% of quarterly initiatives related to Workspace, Learnability, and Foundations
   1. **CEO KR:** First order large SAOs on yearly plan
   1. **CEO KR:** Achieve X% of ARR on cloud licensing


